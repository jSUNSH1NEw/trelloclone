## A Trello Clone using React Hooks and GraphQL

This Repository its a playground for graphql and react trying too build trello clone 

- This Application use React Smooth Dnd for Trello like Drag and Drop. you can also use `react-beautiful-dnd` or `react-dnd`

- I have implemented GraphQL Subscriptions for real time data. you can skip that if you just want to show the changes only for single user.

### Dependencies

### Backend
- graphql
- apollo-server-express
- express
- mongoose

### FrontEnd
- @apollo/react-hooks
- react-smooth-dnd
- styled-components


### To Run the Application

```
  cd react-gaphql-trello-clone
  npm i
  cd server
  npm i
  cd ..
  cd client
  npm i
  cd ..
  npm run app 
```

> Note: This is a playing ground project be free to use it and add some functionnalities 
